package lec03.glab.simplepoly2;

import lec03.glab.simplepoly.MyClass;

import java.awt.*;

public class PolyDriver {

    public static void main(String[] args){

        Object[] objObjects = new Object[6];

        objObjects[0] = new Double(6785.01);
        objObjects[1] = new String("Hello");
        objObjects[2] = new Boolean(true);
        objObjects[3] = new Character('B');
        objObjects[4] = new Rectangle(1,2,5,5);
        objObjects[5] = new MyClass();

        for (Object objObject : objObjects) {
            System.out.println(objObject.toString());
        }
    }
}
