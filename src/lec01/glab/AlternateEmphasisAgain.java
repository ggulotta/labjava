package lec01.glab;

import java.util.Scanner;


public class AlternateEmphasisAgain {

    /**
     Write a program that asks the user for a sentence and then splits the sentence into words, and then
     displays alternating words as UPPERCASE.


     Write the same program, but don't split the setence, rather detect a space and flip a boolean flag
     */
	public static void main(String[] args) {

        Scanner in = new Scanner(System.in);
        System.out.println("Enter a sentence please:");
        String strSentence = in.nextLine();

        boolean bSpaceFound = false;
        strSentence = strSentence.trim();
        for (int nC = 0; nC < strSentence.length(); nC++) {
            char cChar = strSentence.charAt(nC);
            if(cChar == ' '){
                bSpaceFound = !bSpaceFound;
            }

            if(bSpaceFound){
                System.out.print(String.valueOf(cChar).toLowerCase());
            }
            else
            {
                System.out.print((String.valueOf(cChar)).toUpperCase());
            }
        }

    }
}
