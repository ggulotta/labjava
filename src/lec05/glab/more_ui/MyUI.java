package lec05.glab.more_ui;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class MyUI implements ActionListener {
    private JPanel mPanel;
    private JButton mButtonRoll1;
    private JPanel mPanImage;
    private JButton mButtonRoll2;
//    private myListener myListener;

    public static void main(String[] args) {
        JFrame frame = new JFrame("MyUI");
        frame.setContentPane(new MyUI().mPanel);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setSize(1000,1000);
        frame.setVisible(true);
    }

    private void createUIComponents() {
        // TODO: place custom component creation code here

        mPanel = new JPanel();

//        mPanImage = new JPanel();

        mPanImage = new ImgPan(new ImageIcon(System.getProperty("user.dir") + "/src/lec05/glab/more_ui/foo-chan.jpg").getImage());

//        myListener = new myListener();

        mButtonRoll1 = new JButton("1");
        mButtonRoll1.addActionListener(this);
//            @Override
//            public void actionPerformed(ActionEvent e) {
//                System.out.println(this.getClass().toString());
//                JOptionPane.showConfirmDialog(mPanel, "clicked 1");
//            }
//        });

        mButtonRoll2 = new JButton("2");
        mButtonRoll2.addActionListener(this);
//            @Override
//            public void actionPerformed(ActionEvent e) {
//                System.out.println(this.getClass().toString());
//                JOptionPane.showConfirmDialog(mPanel, "clicked 2");
//            }
//        });
    }

    @Override
    public void actionPerformed(ActionEvent e) {

        if(((JButton)e.getSource()).getText().equalsIgnoreCase("Click 1")) {
//            JOptionPane.showConfirmDialog(null, "Clicked 1");
            ((ImgPan) mPanImage).setImg(new ImageIcon(System.getProperty("user.dir") + "/src/lec05/glab/more_ui/maru.jpg").getImage());
        } else {
//            JOptionPane.showConfirmDialog(null, "Clicked 2");
            ((ImgPan) mPanImage).setImg(new ImageIcon(System.getProperty("user.dir") + "/src/lec05/glab/more_ui/foo-chan.jpg").getImage());
        }

//        System.out.println(this.getClass().toString());
        mPanImage.repaint();
    }
}

class ImgPan extends JPanel {

    private Image img;

    public ImgPan(Image img) {
        this.img = img;
        Dimension size = new Dimension(img.getWidth(null), img.getHeight(null));
        setPreferredSize(size);
        setMinimumSize(size);
        setMaximumSize(size);
        setSize(size);
        setLayout(null);
    }

    public Image getImg() {
        return img;
    }

    public void setImg(Image img) {
        this.img = img;
    }

    @Override
    public void paintComponent(Graphics g) {
        g.drawImage(img, 0, 0, null);
    }
}



//class myListener implements ActionListener {
//
//    @Override
//    public void actionPerformed(ActionEvent e) {
//
//        if(((JButton)e.getSource()).getText().equalsIgnoreCase("Click 1")) {
//            JOptionPane.showConfirmDialog(null, "Clicked 1");
//        } else {
//            JOptionPane.showConfirmDialog(null, "Clicked 2");
//        }
//
//        System.out.println(this.getClass().toString());
//    }
//}
